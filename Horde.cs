﻿using System;
using System.Collections.Generic;

namespace Horde
{
    public class Horde
    {
        public List<Mesozoic.Dinosaur> horde = new List<Mesozoic.Dinosaur>();

        public void addDino(Mesozoic.Dinosaur dino)
        {
            horde.Add(dino);
        }

        public void removeDino(Mesozoic.Dinosaur dino)
        {
            horde.Remove(dino);
        }

        public string SayHello()
        {
            string DisSalut = "";
            foreach (Mesozoic.Dinosaur dino in horde)
            {
                DisSalut += dino.sayHello() +"\n";
            }
            return DisSalut;
        }
    }
}
