﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MesozoicSolution
{
   public class Program
    {
        static void Main(string[] args)
        {
            Mesozoic.Dinosaur louis = new Mesozoic.Dinosaur("Louis", "Stegausaurus", 12);
            Mesozoic.Dinosaur nessie = new Mesozoic.Dinosaur("Nessie", "Diplodocus", 11);
            Mesozoic.Dinosaur Jean = new Mesozoic.Dinosaur("Jean", "Diplodocus", 50);
            Mesozoic.Dinosaur Neymar = new Mesozoic.Dinosaur("Neymar", "Stegausaurus", 79);
            Horde.Horde dinosaurs = new Horde.Horde();
            dinosaurs.addDino(louis);
            dinosaurs.addDino(nessie);
            dinosaurs.addDino(Jean);
            dinosaurs.addDino(Neymar);
            Console.WriteLine(dinosaurs.SayHello());
            dinosaurs.removeDino(Jean);
            Console.WriteLine(dinosaurs.SayHello());
            dinosaurs.removeDino(Neymar);
            Console.WriteLine(dinosaurs.SayHello());
            dinosaurs.removeDino(louis);
            Console.WriteLine(dinosaurs.SayHello());
            Console.ReadKey();

        }
    }
}
